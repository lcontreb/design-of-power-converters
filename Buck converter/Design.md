# **A. Specification**

| Description | Value | Unit |
| ------ | ------ | ------ |
| Input voltage (Vi) | 48 | V |
| Input voltage (Vo) | 12 | V |
| Max. Output current (Io) | 50 | V |
| Max. Output voltage ripple () | 1% | - |
| Min. Efficiency (n) | 90% | - |

# **B. Design assumptions**

| Description | Value | Unit |
| ------ | ------ | ------ |
| Operation Mode | CCM | - |
| Switching frequency (fs) | 20 | KHz |
| Max. inductor current ripple () | 20% | - |
| Max. Inductor power losses | 25 | W |
| Max. Semiconductor power losses | 25 | W |

# **C. Steady-state analysis (ideal circuit)**

| Description | Value | Unit |
| ------ | ------ | ------ |
| Duty cycle (D) | 0.25 | - |
| Avg. Inductor current (IL) | 50 | A |
| Peak Inductor current (IL) | 60 | A |
| Min. Inductance (L) | 22.5 | uH |
| Min. Output capacitance (Co) | 0.95 | mF |
| Switch A Min. Blocking voltage | +48 | V |
| Switch A Min. Conduction current | +60 | A |
| Switch B Min. Blocking voltage | +48 | V |
| Switch B Min. Conduction current | -60 | A |

# **D. Selection of ideal devices & components**

| Description | Value | Unit |
| ------ | ------ | ------ |
| Inductance (L) | 24.8 | uH |
| Output capacitance (Co) | 1.89 | mF |
| Switch A Device realization | Transistor | - |
| Switch A Device realization | Diode | - |

# **E. Selection of non-ideal devices & components**

## E.1. Semiconductor devices

### E.1.1. Selection of Switch A

- Electrical/thermal rating analysis

| Features | Option 1| Option 2 | Unit|
| ------ | ------ | ------ |------ |
| Manufacturer | Infineon | Infineon | - |
| Model | IPP023N10N5 | IRLB4030PbF | - |
| Type | Optimus Si N-MOSFET | HEXFET Si N-MOSFET| - |
| Package | PG-TO220-3| TO-220 | - |
| RDSon | 2.3 | 4.3 | mΩ |
| Max. Rth,jc | 0.4 | 0.4 | K/W |
| Max. Rth,ja | 62 | 62 | K/W |
| Max. Junction temperature (Tj) | 175 | 175 | oC|
| Current rise time (tcr) | 26 | 330 | ns|
| Current fall time (tcf) | 29 | 170 | ns|
| Gate charge (Qg) | 168 | 87 | nC|
| Voltage rise time (tvr) | 27.5 |  | ns|
| Voltage fall time (tvf) | 13.8 |  | ns|
| Cost per 1K | 4.07| 2.43 |USD|

- Power loss analysis

| Description | Option 1 | Option 2 | Unit |
| ------ | ------ | ------ | ------ |
| Avg. Conduction power loss | 2.07 | 3.87 | W |
| Avg. Turn-on power loss | 1.54 | 9.50 | W |
| Avg. Turn-off power loss | 1.23 | 4.90 | W|
| Total Avg. power loss | 4.84 | 18.27| W |

### E.1.2. Selection of Switch A's gate driver

| Features | Option 1| Option 2 | Unit|
| ------ | ------ | ------ |------ |
| Manufacturer | Infineon | Infineon | - |
| Model | 1EDN7550/1EDN8550 | 1EDN751x/1EDN851x | - |
| Max. Output current (sink) | 8| 8 | A |
| Rds (sink) | 0.35| 0.35 | Ω |
| Max. Output current (source) | 4 | 4 | A |
| Rds (source) | 0.85 | 0.85 | Ω |
| Delay time | 45 | 19 | ns |
| Max. IQ (high) | 1.1 | 0.4| mA|
| Max. IQ (low) | 0.3 | 0.37| mA|
| Cost per 1K | 0.42|  |USD|

### E.1.3. Selection of Switch A's heat sink

| Features | Option 1| Option 2 | Unit|
| ------ | ------ | ------ |------ |
| Manufacturer | AAVID | CUI DEVICES | - |
| Model | 5750 | HSE-B250-04H | - |
| Material | Thick aluminum | AL 6063-T5
black anodized ||
| Rth,sa | 14.8 | 14.3 | oC/W|
| Length | 12.7 || mm|
| Width | 25.4 || mm|
| Height | 30 || mm|
| Cost per 1K | 0.318 | 0.576 |USD|

## E.2. Magnetic devices

### E.2.1. DC Filter Inductor L

- Design considerations

Design based on the Geometric constant (Kg) method based on dominated winding loss.

| Features | Value | Unit |
| ------ | ------ | ------ |
| Core material | 3F3 | - |
| Max. Flux density (B) @ 100 °C | 0.33 | T |
| Winding material | Cu | - |
| Cu resistance @ 100 °C | 2.3 | uΩ-m |

- Design of magnetic Core

| Features | Option 1| Option 2 | Unit|
| ------ | ------ | ------ |------ |
| Core type | POT |  | - |
| Weight | 104 |  | g |
| Dimensions | 42_29 |  | mm |
| Air-gap length (lg) | 0.467 | | cm |

- Design of winding

| Features | Option 1| Option 2 | Unit|
| ------ | ------ | ------ |------ |
| Turns number | 18 |  | - |
| AWG | 12 |  | - |

- Power loss analysis

| Description | Option 1 | Option 2 | Unit |
| ------ | ------ | ------ | ------ |
| Avg. Winding power loss | 26.9 |  | W |

## E.3. Capacitors

### E.3.1. Output capacitor Co

