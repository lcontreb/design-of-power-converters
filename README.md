# **Design of power converters**

Each directory contains information and files employed to implementent different power converters according to a given set of requirements.

The name of each directory indicates the topology type of the designed converter.

The file `Design.md` provides a report of the converter implementation.

The directory `sim` contains the files used to simulate and validate the functionality and performance of the converters.